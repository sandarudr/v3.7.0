# Verifying Policies Applied on an iOS Device

There will be situations where you will want to verify the policies that have been enforced on the iOS devices in your Organization. In such situations follow the steps given below to identify the policies that have been enforced on an iOS device as opposed to [getting the policy details from device details page in the device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813583/Policy+Management#PolicyManagement-VVerifythepolicyenforcedonadevice).

1.  Tap **Settings** > **General** > **Profiles** or** Profiles & Device Management**.   
2.  Tap the respective profile you configured to run with the Entgra IoT Server that is under **MOBILE DEVICE MANAGEMENT**.  
    Example: In this scenario tap WSO2QA Company Mobile Device Management.
3.  Tap **Restrictions** to view the restrictions that have been enforced on the device via the Entgra IoT Server.

    ![image](352824319.png)

    The restrictions that have been enforced on the device via Entgra IoT Server will be shown to you. 

    

    

    In this example, the restrictions policy has been enforced on the device. For more information on the Entgra IoT Server policy enforcement criteria, see [Key concepts](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352814160/Key+Concepts#KeyConcepts-Policyenforcementcriteria).

    

    

    ![image](352824330.png)
