# Android Remote app Configuration

This section descrbies the **Remote app Configuration** functionality possessed by the Entgra IoTS Android Agent. 

This allows restrictions to be enforced on a per app basis for specific devices via the server.

## Running the Remote app Config operation

1. Go to the **Device List page**

    ![image](2.png)

2. Select the particular device of choice

    ![image](3.png)
    
3. Head over to the section with the **Device Operations**, it should appear as follows;

    ![image](1.png)

4. Select the **Send app restriction** option. A modal will popup as shown below.

    ![image](4.png)

5. Enter the Application package name and configuration option required.
6. Click **Send to device** to run the operation.
7. Wait a couple of seconds for execution. 

{{< hint info >}}
    Check Entgra IoTS logs in case there is a prolonged delay or unexpeted error.
{{< /hint >}}
