---
bookCollapseSection: true
weight: 5
---

# <strong> Virtual Private Network </strong>

### VPN Settings

VPNs allow devices that aren’t physically on a network to securely access the network. Configure 
the OpenVPN settings on Android devices. In order to enable this, device needs to have "OpenVPN for Android" application installed.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>OpenVPN Server Config</strong></td>
           <td> </td>
        </tr>
    </tbody>
</table>

### Always On VPN Settings

Android can start a VPN service when the device boots and keep it running while the device is on. 
This feature is called always-on VPN and is available in Android 7.0 (API Level 24) or higher. 
Configure an always-on VPN connection through a specific VPN client application

<i> Below configurations are valid only when the Agent is work-profile owner or device owner.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>VPN Client Application Package Name</strong></td>
           <td>Package name of the VPN client application to be configured.</td>
        </tr>
    </tbody>
</table>

