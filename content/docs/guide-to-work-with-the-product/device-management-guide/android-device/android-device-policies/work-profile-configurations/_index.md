---
bookCollapseSection: true
weight: 6
---

# <strong> Work-Profile Configurations </strong>

The configurations below can be applied to the devices where the agent is running in Android Work-Profile.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Profile Name</strong></td>
            <td>Name of the Work-Profile created by IoT Server Agent</td>
        </tr>
        <tr>
            <td><strong>Enable System Apps</strong></td>
            <td>The system applications that needs to enabled in the work-profile.<br><i>Should be exact package names seperated by commas. Ex: com.google.android.apps
            .maps, com.google.android.calculator.</i></td>
        </tr>
        <tr>
            <td><strong>Hide System Apps</strong></td>
            <td>The system applications that needs to be hidden in the work-profile.<br><i>Should be 
            exact package names seperated by commas. Ex: com.google.android.apps
            .maps, com.google.android.calculator.</i></td>
        </tr>
        <tr>
            <td><strong>Unhide System Apps</strong></td>
            <td><i>Should be exact package names seperated by commas. Ex: com.google.android.apps
            .maps, com.google.android.calculator.</i></td>
        </tr>
        <tr>
            <td><strong>Enable Google Play Store Apps</strong></td>
            <td>The applications that needs to be downloaded and installed from Google play store
             to the work-profile.<br><i>Should be exact package names seperated by commas. Ex: com
             .google.android.apps
            .maps, com.google.android.calculator.</i></td>
        </tr>
    </tbody>
</table>