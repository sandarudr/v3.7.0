---
bookCollapseSection: true
weight: 10
---

# <strong> System Update Policy (COSU) </strong>

This configuration can be used to set a passcode policy to an Android Device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.
 
<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>System Update</strong>
                    <br>Type of the System Update to be set by the Device Owner
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul style="list-style-type:disc;">
                    <strong><li>Automatic</li></strong>
                    <strong><li>Postpone</li></strong>
                    <li><strong>Window</strong>
                        <ul>
                            <li><i>Below configuration of start time and end time are 
                                     valid only when window option is selected.</i>
                                <li><strong>Start Time</strong> : Window start time for system update</li>
                                <li><strong>End Time</strong> : Window end time for system update</li>
                        </ul>
                        </li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>