---
bookCollapseSection: true
weight: 9
---

# <strong> Runtime Permission Policy (COSU / Work Profile) </strong>

This configuration can be used to set a runtime permission policy to an Android Device.

 <i>Already granted or denied permissions are not affected by this policy.
 Permissions can be granted or revoked only for applications built with a Target SDK Version of 
 Android Marshmallow or later.</i>
 
 
<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Set default runtime permission</strong></td>
            <td>When an app requests a runtime permission, this enforces whether the user needs 
            to prompted or the permission (PROMPT USER) either automatically granted (AUTO GRANT)
             or denied (AUTO DENY)
            .</td>
        </tr>
        <tr>
            <td colspan="2"><center><strong>Set app-specific runtime 
            permissions</strong></ceter></td>
        </tr>
        <tr>
            <td><strong>Application</strong></td>
            <td>Eg: [ Android Pay ]</td>
        </tr>
        <tr>
            <td><strong>Package Name</strong></td>
            <td>Eg: [ com.google.android.pay ]</td>
        </tr>
        <tr>
            <td><strong>Permission Name</strong></td>
            <td>Eg: [ android.permission.NFC ]</td>
        </tr>
        <tr>
            <td><strong>Permission Type</strong></td>
            <td>
                <ul>
                    <li>PROMPT USER</li>
                    <li>AUTO GRANT</li>
                    <li>AUTO DENY</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>