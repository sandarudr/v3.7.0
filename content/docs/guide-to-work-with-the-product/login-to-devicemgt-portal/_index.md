---
bookCollapseSection: true
weight: 2
---

# Login to devicemgt portal

Device management portal app in the management related web application that EMM admins may use to manage devices. To login to this portal, 

### Step 1
Visit the devicemgt app using your browser. This can be accessed with the following url format, https://Replace-Your-IP-Address:9443/devicemgt For example https://localhost:9443/devicemgt

### Step 2
You will get a security exception like bellow, since SSL certificates are not setup. Click "Advanced" then  "Proceed to localhost (unsafe)"

![image](1.png)

### Step 3
Browser will be redirected to the login page. Type the default username "admin" and the password "admin" and click login.

![image](2.png)

### Step 4
Browser will be redirected to the device management portal which looks like bellow.

![image](3.png)

