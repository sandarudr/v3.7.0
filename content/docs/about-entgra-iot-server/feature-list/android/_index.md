---
bookCollapseSection: true
weight: 1
---

# Android features


## Operations

One time operations that can be executed on the devices are useful for runtime maintenance of devices.
<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Feature</th><th>Legacy</th><th>Work Profile</th><th>Dedicated (Kiosk)</th><th>Fully managed (COPE)</th><th>System app</th><th>Description</th></tr></thead><tbody>
 <tr><td>Get Device Information</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Fetch the device's runtime information.</td></tr>
 <tr><td>Get Installed Applications</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Fetch the device's current location</td></tr>
 <tr><td>Get Device Location Information</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Fetch the device's installed application list </td></tr>
 <tr><td>Ring Device</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Ring the device for the purpose of locating the device in case of misplace.</td></tr>
 <tr><td>Upload File</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Feature is in development. Upload file to a specific folder on the device</td></tr>
 <tr><td>Download File</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Feature is in development. Download file on a specific folder on the device</td></tr>
 <tr><td>Mute Device</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Put the device in silense mode.</td></tr>
 <tr><td>Change Lock Code</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Changes the device's currently set lock code. From Android N upwards, clear passcode will not work</td></tr>
 <tr><td>Clear Password</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Remove any password that the device owner has put. From Android N upwards, clear passcode will not work</td></tr>
 <tr><td>Send Notifications/Messages</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Send a notification(message) to the device</td></tr>
 <tr><td>Enterprise Wipe</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Wipe the entreprise portion of the device</td></tr>
 <tr><td>Wipe Device(Factory reset)</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Factory reset a device.</td></tr>
 <tr><td>Device Lock(soft lock)</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Lock the device remotely. Similar to pressing the power button on the device and locking it.</td></tr>
 <tr><td>Reboot Device</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Restart the phone for example for troubleshooting purposes.</td></tr>
 <tr><td>Upgrade Firmware</td><td>✕</td><td>✕</td><td>✕</td><td>✕</td><td>✓</td><td>Upgrade the Android operating firmware and the firmware and the device has to be compatiple and only applicable in OEM scenarios</td></tr>
 <tr><td>Execute Shell Command</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Remotely execute the shell commands on the device's command prompt.</td></tr>
 <tr><td>Hard lock</td><td>✓</td><td>&nbsp;</td><td>✓</td><td>✓</td><td>✓</td><td>Lock a device remotly by and admin and only the admin can unlock the device.</td></tr>
 <tr><td>Manage Web Clip</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Install a shortcut link to a web page/web app on the phone's home screen</td></tr>
 <tr><td>Trigger Google Play App</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Install an app from the google play store</td></tr>
 <tr><td>Install/Uninstall/update applications</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Capability to perform various application management tasks sush as install, uninstall and update apps</td></tr>
 <tr><td>View device screen</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Screen sharing with the Admin</td></tr>
 <tr><td>Remote Control Device</td><td>✓</td><td>✓</td><td>✓</td><td>&nbsp;</td><td>✓</td><td>Allow Admin to remotely control the device</td></tr>
 <tr><td>Get Logcat</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>View the log of the operating system</td></tr>
 <tr><td>Silent App Install</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Install apps on the device without promting the user to click install.</td></tr>
 <tr><td>Remote kiosk enable</td><td>✕</td><td>✕</td><td>✓</td><td>✕</td><td>✕</td><td>Enable or disable kiosk mode remotely for maintainance reasons, troubleshooting etc.</td></tr>
</tbody></table>

## Policies

The policies that can be applied on an Android device depends on the way the device is enrolled with the server. 


<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Feature</th><th>Legacy</th><th>Work Profile</th><th>Dedicated / Fully Managed / System</th><th>Fully managed (COPE)</th><th>System app</th><th>Description</th></tr></thead><tbody>
 <tr><td>Passcode Policy</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Add a passcode strength policy to the device or to work profile</td></tr>
 <tr><td>Encrypt Storage</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Execute the encypt device storage.</td></tr>
 <tr><td>Configure Wifi</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Push a configuration contaning the wifi profile of the company.</td></tr>
 <tr><td>Configure VPN</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Push a configuration contaning the VPN profile of the company.</td></tr>
 <tr><td>Work Profile Configurations</td><td>✕</td><td>✓</td><td>✕</td><td>✕</td><td>✕</td><td>Decides which system apps must be enabled or disabled in a work profile</td></tr>
 <tr><td>Dedicated Device Configurations(Kiosk)</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Configure the behaviour of the Kiosk</td></tr>
 <tr><td>Blacklist/Whitelist Apps</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Decides which apps are allowed do be in a device</td></tr>
 <tr><td>Runtime permissions</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Permissions app require to work can be uptomatically granted and locked.</td></tr>
 <tr><td>Firmware Update Policy</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Specify the strategy or the time windows to perform OS updates</td></tr>
 <tr><td>Monitor/Revoke Policies</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Continuously monitor the policies of the device to detect any policy violations.</td></tr>
 <tr><td>Android-Certificate Install </td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Install certificate to devices remotely</td></tr>
 <tr><td>Proxy settings</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Reroute all the http communication of a device via a global http proxy</td></tr>
 <tr><td>Enrollment app install</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Decides which apps needs to be installed upon enrollment</td></tr>
 <tr><td>Remote App configurations</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Send the app configurations for user's installed apps</td></tr>
 <tr><td>Disallow removal of profile</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disable the user's ability to unenroll from EMM</td></tr>
</tbody></table>

### Restrictions Policy

There are large number of restrictions that can be applied on the device. Restrictions are a policy and following describes all the restrictions available.

<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Feature</th><th>Legacy</th><th>Work Profile</th><th>Dedicated / Fully Managed / System</th><th>Fully managed (COPE)</th><th>System app</th><th>Description</th></tr></thead><tbody>
 <tr><td>Allow use of camera</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow the access to camera</td></tr>
 <tr><td>Disallow modifying certificates</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow modifying installed certificates in the device</td></tr>
 <tr><td>Disallow configuring VPN</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disable configuring VPN settings</td></tr>
 <tr><td>Disallow configuring app control</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Hide the status bar of </td></tr>
 <tr><td>Disallow crossprofile copy paste</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Copying text between profiles is blocked</td></tr>
 <tr><td>Disallow debugging</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disable usb debuging</td></tr>
 <tr><td>Disallow install apps </td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow installing apps to the device</td></tr>
 <tr><td>Disallow install from unknown sources</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow installing apps from unknown sources</td></tr>
 <tr><td>Disallow modify accounts</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow modifying accounts such as Google, Facebook from being modified/ added/ removed</td></tr>
 <tr><td>Disallow outgoing beams</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disable using NFC to transfer data</td></tr>
 <tr><td>Disallow location sharing</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disable sharing device location</td></tr>
 <tr><td>Disallow uninstall apps</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disable uninstalling apps</td></tr>
 <tr><td>Disallow parent profile app linking</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow apps in the personal profile to handle web links from the work profile</td></tr>
 <tr><td>Ensure verifying apps</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Enforce only verified apps can be installed on the device</td></tr>
 <tr><td>Disable screen capture</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow capturing the screen of the device</td></tr>
 <tr><td>Enable auto timing</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Enable or diable using time from mobile network as system time</td></tr>
 <tr><td>Disallow  SMS</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable access to SMS</td></tr>
 <tr><td>Disallow volume adjust</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable adjusting the volume of the device</td></tr>
 <tr><td>Disallow cell broadcast</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disables cell broadcasting messages of the network</td></tr>
 <tr><td>Disallow configuring bluetooth</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable configuring bluetooth settings</td></tr>
 <tr><td>Disallow configuring mobile networks</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable configuring moble network settings</td></tr>
 <tr><td>Disallow configuring tethering</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable configuring tethering settings</td></tr>
 <tr><td>Disallow configuring WiFi</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable configuring WiFi settings</td></tr>
 <tr><td>Disallow  safe boot</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow booting into safe mode</td></tr>
 <tr><td>Disallow outgoing calls</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable outgoing calls</td></tr>
 <tr><td>Disallow mount physical media</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable plugining in different media devices</td></tr>
 <tr><td>Disallow create window</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable showing certain notifications, toasts and alert by apps</td></tr>
 <tr><td>Disallow factory reset</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable factory resetting of devices</td></tr>
 <tr><td>Disallow remove user</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow adding new users to device</td></tr>
 <tr><td>Disallow add user</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow removing users from device</td></tr>
 <tr><td>Disallow network reset</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow user from performing network setting reset</td></tr>
 <tr><td>Disallow USB file transfer</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow transfering data over USB</td></tr>
 <tr><td>Disallow unmute microphone</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Configure access to microphone</td></tr>
 <tr><td>Disallow status bar</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Block user from opening the notification bar and access to status bar</td></tr>
 <tr><td>Disallow set wallpaper</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow changing wallpaper</td></tr>
 <tr><td>Disallow auto fill</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow auto filling forms</td></tr>
 <tr><td>Disallow bluetooth</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow bluetooth</td></tr>
 <tr><td>Disallow bluetooth sharing</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disable sharing via bluetooth</td></tr>
 <tr><td>Disallow data roaming</td><td>✕</td><td>✕</td><td>✓</td><td>✓</td><td>✓</td><td>Disallow data roaming</td></tr>
</tbody></table>

